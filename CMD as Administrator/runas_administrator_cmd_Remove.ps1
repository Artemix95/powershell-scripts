﻿# Delete the registry keys for the context menu entry in regular folders
$regPath = "Registry::HKEY_CLASSES_ROOT\Directory\shell\runas"
Remove-Item -Path $regPath -Force

$regBackgroundCmdPath = "Registry::HKEY_CLASSES_ROOT\Directory\Background\shell\runas\command"
Remove-Item -Path $regBackgroundCmdPath -Force
