﻿# creates registry key to define the context menu entry for regular folders
$regPath = "Registry::HKEY_CLASSES_ROOT\Directory\shell\runas"
New-Item -Path $regPath -Force | Out-Null

# set the name shown in the context menu
Set-ItemProperty -Path $regPath -Name "(Default)" -Value "Open CMD as Administrator" -Type String

# creates another registry key to define the command to execute
$regCmdPath = "Registry::HKEY_CLASSES_ROOT\Directory\shell\runas\command"
New-Item -Path $regCmdPath -Force | Out-Null

#  this command opens a Command Prompt window at the selected folder location
Set-ItemProperty -Path $regCmdPath -Name "(Default)" -Value "cmd.exe /s /k pushd `"%V`"" -Type String


# same as before but apply the context menu when right-clicking on the background of a folder
$regBackgroundPath = "Registry::HKEY_CLASSES_ROOT\Directory\Background\shell\runas"
New-Item -Path $regBackgroundPath -Force | Out-Null
Set-ItemProperty -Path $regBackgroundPath -Name "(Default)" -Value "Open CMD as Administrator" -Type String

$regBackgroundCmdPath = "Registry::HKEY_CLASSES_ROOT\Directory\Background\shell\runas\command"
New-Item -Path $regBackgroundCmdPath -Force | Out-Null
Set-ItemProperty -Path $regBackgroundCmdPath -Name "(Default)" -Value "cmd.exe /s /k pushd `"%V`"" -Type String
