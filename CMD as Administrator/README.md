# Open CMD as Administrator - Context Menu Script
This script is designed to create a context menu entry that allows users to open a Command Prompt window with administrator privileges directly from the context menu when right-clicking on folders in Windows. It adds the "Open CMD as Administrator" option to the context menu for regular folders and the background of folders.
![Alt text](image.png)

## How to Use
Note: Modifying the Windows Registry can have serious consequences. Proceed with caution and create a backup before making any changes.

### Add Context Menu Entry
Execute the following Powershell script with Administrator privileges
`./runas_administrator_cmd.ps1`

### Remove Context Menu Entry
If you wish to remove the "Open CMD as Administrator" option from the context menu, you can use the following script with Administrator privileges:
`./runas_administrator_cmd_Remove.ps1`

## Important Notes

- Make sure you have a backup of your registry before running the script or making any changes.
- These modifications only apply to the Windows context menu for folders.
- Use these scripts responsibly and only on systems where you have the necessary permissions to make changes to the Windows Registry.

**Disclaimer: The use of these scripts is at your own risk. Always exercise caution when modifying the Windows Registry, as improper changes can lead to system instability or other issues.**