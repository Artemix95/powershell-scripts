# Download and Installation Scripts
This folder contains a collection of PowerShell scripts to download and install various software packages on Windows. These scripts aim to simplify the process of downloading and installing popular tools by automating the required steps. Below are brief descriptions of each script and the software it installs:

## firefox.ps1
- Description: This script downloads and silently installs the latest version of Mozilla Firefox web browser for 64-bit Windows.
- Usage: Simply run the script, and it will handle the download and installation process automatically.

## git.ps1
- Description: This script downloads and silently installs the latest version of Git for Windows.
- Usage: Execute the script, and it will handle the download and installation process automatically. Git will also be added to the system's PATH environment variable.

## java.ps1
- Description: This script downloads and installs the Java JDK 11 (Java Development Kit) for 64-bit Windows.
- Usage: Run the script, and it will handle the download, installation, and configuration of environment variables, including setting JAVA_HOME.

## neo4j_installation.ps1
- Description: This script downloads and installs the latest LTS (Long-Term Support) version of Neo4j for Windows.
- Usage: Execute the script, and it will handle the download, extraction, and installation of Neo4j. The Neo4j service will be started automatically after installation.

## notepad++.ps1
- Description: This script downloads and silently installs the latest version of Notepad++ text editor for Windows.
- Usage: Run the script, and it will handle the download and installation process automatically.

## python.ps1
- Description: This script downloads and installs the latest version of Python for Windows.
- Usage: Execute the script, and it will handle the download, installation, and configuration of environment variables, including adding Python to the system's PATH.

## Usage Instructions:
1. Clone or download the repository to your local machine.
2. Open a PowerShell window with Administrator privileges.
3. Navigate to the directory where the downloaded scripts are located.
4. Run the desired script by executing its filename (e.g., .\firefox.ps1).

### Note:
- Always execute PowerShell scripts from trusted sources to ensure security and avoid potential risks.
- Make sure to have an active internet connection for the download process.
- Some scripts may require administrative privileges for installation.
- Read the script comments for additional instructions and details.

## Disclaimer:
These scripts are provided as-is and without any warranties. The authors and contributors of this repository are not responsible for any issues or damages that may arise from using these scripts. Use them responsibly and at your own risk.