﻿# Define the URL to download the latest Python installer (Change if URL changes)
$pythonDownloadUrl = "https://www.python.org/ftp/python/3.9.7/python-3.9.7-amd64.exe"

# Define the installation directory (Change if needed)
$installDirectory = "C:\Python39"

# Download the latest Python installer
Write-Host "Downloading the latest Python installer..."
$pythonInstallerPath = Join-Path $env:TEMP "python_installer.exe"
Invoke-WebRequest -Uri $pythonDownloadUrl -OutFile $pythonInstallerPath

# Install Python
Write-Host "Installing Python..."
Start-Process -Wait -FilePath $pythonInstallerPath -ArgumentList "/quiet", "InstallAllUsers=1", "PrependPath=1", "Include_test=0", "Include_launcher=0", "Include_doc=0", "Include_tcltk=0", "Include_pip=1", "TargetDir=$installDirectory"

# Add Python to the system PATH environment variable
Write-Host "Configuring environment variables..."
$pythonPath = Join-Path $installDirectory "python.exe"
$pythonScriptsPath = Join-Path $installDirectory "Scripts"
$existingPath = [Environment]::GetEnvironmentVariable("Path", [EnvironmentVariableTarget]::Machine)
$newPath = $existingPath + ";$pythonPath;$pythonScriptsPath"
[Environment]::SetEnvironmentVariable("Path", $newPath, [EnvironmentVariableTarget]::Machine)

# Refresh the environment variables for the current session
$env:Path = $newPath

# Display Python and pip versions
Write-Host "Python has been installed successfully."
Write-Host "Python version:"
python --version
Write-Host "pip version:"
pip --version

# Clean up the installer file
Remove-Item $pythonInstallerPath