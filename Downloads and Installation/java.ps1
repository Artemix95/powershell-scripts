﻿# Define the URL to download the latest Java JDK installer (Change if URL changes)
$javaDownloadUrl = "https://download.oracle.com/java/11/latest/jdk-11_windows-x64_bin.exe"

# Define the installation directory (Change if needed)
$installDirectory = "C:\Java"

# Download the latest Java JDK installer
Write-Host "Downloading the latest Java JDK installer..."
$javaInstallerPath = Join-Path $env:TEMP "jdk_installer.exe"
Invoke-WebRequest -Uri $javaDownloadUrl -OutFile $javaInstallerPath

# Install Java JDK
Write-Host "Installing Java JDK..."
Start-Process -Wait -FilePath $javaInstallerPath -ArgumentList "/s", "INSTALLDIR=$installDirectory"

# Add Java JDK to the system PATH environment variable
Write-Host "Configuring environment variables..."
$javaPath = Join-Path $installDirectory "bin"
$existingPath = [Environment]::GetEnvironmentVariable("Path", [EnvironmentVariableTarget]::Machine)
$newPath = $existingPath + ";$javaPath"
[Environment]::SetEnvironmentVariable("Path", $newPath, [EnvironmentVariableTarget]::Machine)

# Set JAVA_HOME environment variable
$javaHome = $installDirectory
[Environment]::SetEnvironmentVariable("JAVA_HOME", $javaHome, [EnvironmentVariableTarget]::Machine)

# Refresh the environment variables for the current session
$env:Path = $newPath
$env:JAVA_HOME = $javaHome

# Display Java JDK version
Write-Host "Java JDK has been installed successfully."
Write-Host "Java JDK version:"
java -version

# Clean up the installer file
Remove-Item $javaInstallerPath