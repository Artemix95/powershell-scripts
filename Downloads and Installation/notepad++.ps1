﻿# Define the download URL for the latest Notepad++ installer (update if necessary)
$downloadUrl = "https://github.com/notepad-plus-plus/notepad-plus-plus/releases/download/v8.5.4/npp.8.5.4.Installer.x64.exe"  # Replace this with the latest version download link

# Define the path where the installer will be downloaded
$installerPath = "$env:TEMP\npp_installer.exe"

# Download the installer
Invoke-WebRequest -Uri $downloadUrl -OutFile $installerPath

# Install Notepad++ silently
Start-Process -FilePath $installerPath -ArgumentList "/S" -Wait

# Clean up the downloaded installer
Remove-Item $installerPath -Force
