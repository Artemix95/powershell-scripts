﻿# Define the download URL for Firefox
$firefoxUrl = "https://download.mozilla.org/?product=firefox-latest&os=win64&lang=en-US"

# Define the path where the Firefox installer will be downloaded
$installerPath = "$env:TEMP\FirefoxInstaller.exe"

# Download the Firefox installer
Invoke-WebRequest -Uri $firefoxUrl -OutFile $installerPath

# Define the silent installation arguments
$installArgs = "/S /D=`"C:\Program Files\Mozilla Firefox`""

# Install Firefox silently
Start-Process -Wait -FilePath $installerPath -ArgumentList $installArgs

# Remove the Firefox installer
Remove-Item $installerPath