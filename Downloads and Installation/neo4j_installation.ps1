﻿# Define the download URL for the latest LTS version of Neo4j (update if necessary)
$neo4jDownloadUrl = "https://dist.neo4j.org/neo4j-community-5.10.0-windows.zip"

# Define the destination folder for downloading and installing Neo4j (update if necessary)
$downloadFolder = "$env:TEMP\Neo4jDownload"
$installDirectory = "C:\Neo4j"

# Create the destination folder for download if it does not exist
if (-Not (Test-Path -Path $downloadFolder -PathType Container)) {
    New-Item -ItemType Directory -Path $downloadFolder | Out-Null
}

# Download the Neo4j installer
Write-Host "Downloading the latest Neo4j installer..."
$zipFilePath = Join-Path $downloadFolder "neo4j.zip"
Invoke-WebRequest -Uri $neo4jDownloadUrl -OutFile $zipFilePath

# Verify that the installer file has been downloaded successfully
if (Test-Path $zipFilePath -PathType Leaf) {
    Write-Host "Neo4j installer downloaded successfully."

    # Extract the contents of the zip installer to the destination folder
    Write-Host "Extracting Neo4j installer..."
    Expand-Archive -Path $zipFilePath -DestinationPath $downloadFolder -Force

    # Move the extracted contents to the defined installation folder and perform the installation
    Write-Host "Installing Neo4j..."
    $extractedFolder = Join-Path $downloadFolder (Get-ChildItem -Path $downloadFolder -Directory | Select-Object -First 1)
    $arguments = "/E", "/MOVE", "/W:1", "/R:5", "/LOG:robocopy_log.txt"
    Copy-Item -Path $extractedFolder -Destination $installDirectory -Recurse

  
    $binfolder = Join-Path $installDirectory "bin"
    Set-Location $binfolder
    Start-Process -FilePath ".\neo4j.bat" -ArgumentList "windows-service install" -Verb RunAs -Wait
   
    # Clean up the temporary download folder
    Remove-Item $zipFilePath -Force
    Remove-Item $downloadFolder -Recurse -Force

    Write-Host "Neo4j has been installed successfully."
} else {
    Write-Host "Failed to download Neo4j installer."
}

# Execute the "net start neo4j" command
$output = Start-Process -FilePath "cmd.exe" -ArgumentList " /c net start neo4j" -Verb RunAs -Wait


# Check if the Neo4j service is active
$serviceName = "neo4j"
$neo4jService = Get-Service -Name $serviceName

if ($neo4jService.Status -eq "Running") {
    Write-Host "The Neo4j service is active and running."
} else {
    Write-Host "The Neo4j service is not running or not installed."
}