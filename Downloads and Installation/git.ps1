﻿# Define the download URL for Git (update if necessary)
$gitUrl = "https://github.com/git-for-windows/git/releases/download/v2.32.0.windows.2/Git-2.32.0.2-64-bit.exe"

# Define the path where the Git installer will be downloaded
$installerPath = "$env:TEMP\GitInstaller.exe"

# Download the Git installer
Invoke-WebRequest -Uri $gitUrl -OutFile $installerPath

# Define the path where Git will be installed
$installDir = "C:\Program Files\Git"

# Define the silent installation arguments
$installArgs = "/SILENT /NORESTART /CLOSEAPPLICATIONS /RESTARTAPPLICATIONS /DIR=`"$installDir`""

# Install Git silently
Start-Process -Wait -FilePath $installerPath -ArgumentList $installArgs

# Remove the Git installer
Remove-Item $installerPath

# Define the path to the Git installation directory
$gitInstallDir = "C:\Program Files\Git"

# Add Git to the system's PATH environment variable
$existingPath = [Environment]::GetEnvironmentVariable("Path", "Machine")
$newPath = "$existingPath;$gitInstallDir\bin;$gitInstallDir\cmd"
[Environment]::SetEnvironmentVariable("Path", $newPath, "Machine")

# Inform the user that Git has been added to the PATH
Write-Host "Git has been added to the system's PATH environment variable."
Write-Host "You can now use Git from the command prompt (CMD)."